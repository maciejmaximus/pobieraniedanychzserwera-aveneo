﻿using ŁadowanieDanychzSerwera2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ŁadowanieDanychzSerwera2.Controllers
{
    public class HomeController : Controller
    {
        DaneDBContext db = new DaneDBContext();

        public ActionResult Index()
        {
            //Dane dane = new Dane();
            //dane.Nazwa = "FHU Kiona";
            //dane.Numer = 13;
            //dane.Miasto = "Otorowo";
            //dane.Ulica = "Szamotulska";
            //dane.KodPocztowy = "64-551";

            //db.Danes.Add(dane);
            //db.SaveChanges();
            return View();
        }

        public JsonResult GetData(string prefix)
        {

            string newPrefix = string.Empty;
            Dane firma = new Dane();

            if (prefix.Length == 13)
            {
                prefix = prefix.Replace("-", "");
            }
            else if (prefix.Length == 12)
            {
                prefix = prefix.Replace("PL", "");
            }

            using (DaneDBContext dbcon = new DaneDBContext())
            {
                if (prefix.Length == 10)
                {
                    firma = dbcon.Danes.Where(a => a.NIP == prefix).FirstOrDefault();
                    if (firma == null)
                    {
                        firma = dbcon.Danes.Where(a => a.KRS == prefix).FirstOrDefault();
                    }
                }
                else
                {
                    firma = dbcon.Danes.Where(a => a.REGON == prefix).FirstOrDefault();
                }
            }

            return new JsonResult { Data = firma, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

            public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}