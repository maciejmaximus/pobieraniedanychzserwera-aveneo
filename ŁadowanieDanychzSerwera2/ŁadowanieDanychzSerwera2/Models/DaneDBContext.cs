﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ŁadowanieDanychzSerwera2.Models
{
    public class DaneDBContext : DbContext
    {
        public DbSet<Dane> Danes { get; set; }
        public DbSet<Log> Logs { get; set; }
    }
}