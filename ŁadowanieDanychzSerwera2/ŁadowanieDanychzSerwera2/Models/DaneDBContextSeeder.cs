﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ŁadowanieDanychzSerwera2.Models
{
    public class DaneDBContextSeeder : DropCreateDatabaseIfModelChanges<DaneDBContext>
    {
        protected override void Seed(DaneDBContext context)
        {
            Dane dane1 = new Dane()
            {
                Nazwa = "Przedsiębriostwo Handlowo Usługowe ABC",
                Ulica = "Morska",
                Numer = 11,
                KodPocztowy = "61-123",
                Miasto = "Poznań",
                NIP = "7777777777",
                KRS = "1111222333",
                REGON = "999955555"
            };
            context.Danes.Add(dane1);
            base.Seed(context);
        }
    }
}