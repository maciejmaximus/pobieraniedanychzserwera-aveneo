﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ŁadowanieDanychzSerwera2.Models
{
    public class Dane
    {
        public int Id { get; set; }
        public string Nazwa { get; set; }
        public string Ulica { get; set; }
        public int Numer { get; set; }
        public string KodPocztowy { get; set; }
        public string Miasto { get; set; }
        public string NIP { get; set; }
        public string KRS { get; set; }
        public string REGON { get; set; }
    }
}