﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ŁadowanieDanychzSerwera2.Models
{
    public class Log
    {
        public int Id { get; set; }
        public string Zapytanie { get; set; }
        public DateTime DataiGodzina { get; set; }
    }
}